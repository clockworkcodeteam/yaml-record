package net.clockworkcode.yamlrecord;

import java.beans.ConstructorProperties;
import java.math.BigDecimal;

/**
 * Created by david on 20/02/2016.
 */
public class Data {
    public static Data NULL_DATA = new Data("", BigDecimal.ZERO, "");
    private String label;

    private String unit;

    private BigDecimal value;

    public BigDecimal getValue() {
        return value;
    }

    public String getLabel() {
        return label;
    }

    @ConstructorProperties({"label", "value", "unit"})
    public Data(String label, BigDecimal value, String unit) {
        this.label = label;
        this.value = value;
        this.unit = unit;
    }

//    public void setLabel(String label) {
//        this.label = label;
//    }
//
//    public void setUnit(String unit) {
//        this.unit = unit;
//    }
//
//    public void setValue(BigDecimal value) {
//        this.value = value;
//    }

    public String getUnit() {
        return unit;
    }

    public Data() {

    }

    @Override
    public String toString() {
        return "Data{" +
                "label='" + label + '\'' +
                ", unit='" + unit + '\'' +
                ", value=" + value +
                '}';
    }
}
