package net.clockworkcode.yamlrecord;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by david on 28/02/2016.
 */
public class RecordConverter {
    public static String toYaml(Record record) {
        Map<String, Object> properties = toMapRepresentation(record);
        return toYamlString(properties);
    }

    public static Record toRecord(String yaml) {
        Map loadedMapRepresentation = (Map)new Yaml().load(yaml);

        ZonedDateTime recordDate = ZonedDateTime.parse((String)loadedMapRepresentation.get("Date"));
        String recordType = (String) loadedMapRepresentation.get("Type");

        Record record = new Record(recordType, recordDate);
        record.setDataList(toDataList(loadedMapRepresentation));
        return record;
    }

    private static List<Data> toDataList(Map loadedMapRepresentation) {
        List<Map<String, Object>> dataListMaps = (List<Map<String, Object>>)loadedMapRepresentation.get("Data");
        return (List<Data>) dataListMaps.stream()
                .map(RecordConverter::toDataItem)
                .filter(i -> !i.getLabel().equals(""))
                .collect(Collectors.toList());
    }

    private static Map<String, Object> toMapRepresentation(Record record) {
        Map<String, Object> properties = new LinkedHashMap<>();
        properties.put("Type", record.getType());
        properties.put("Date", DateTimeFormatter.ISO_INSTANT.format(record.getDateTime()));
        properties.put("System", DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(record.getDateTime()));
        List<Map<String, Object>> collect = (List<Map<String, Object>>) record.getDataList().stream()
                .map(RecordConverter::toDataMap)
                .collect(Collectors.toList());
        properties.put("Data", collect);
        return properties;
    }

    private static String toYamlString(Map<String, Object> properties) {
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setExplicitStart(true);
        Yaml yaml = new Yaml(options);
        return yaml.dump(properties);
    }

    private static Map<String, Object> toDataMap(Data data) {
        Map<String, Object> dataMap = new LinkedHashMap<>();
        dataMap.put("Label", data.getLabel());
        dataMap.put("Value", data.getValue());
        dataMap.put("Unit", data.getUnit());
        return dataMap;
    }

    private static Data toDataItem(Map<String, Object> items) {
        Data data = null;
        try {
            data = new Data((String) items.get("Label"),
                    BigDecimal.valueOf((double) items.get("Value")),
                    (String) items.get("Unit"));
        } catch (Exception e) {
            return Data.NULL_DATA;
        }
        return data;
    }

}
