package net.clockworkcode.yamlrecord;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by david on 20/02/2016.
 *
 ---
 type: instant
 dateTime: 2014-12-28T12:00:00.000Z
 time: 22:00
 data:
    -   label: Pin 1
        value: 245
        unit: W
    -   label: Pin 2
        value: 345
        unit: W
 */
public class Record {
    ZonedDateTime dateTime;
    String system;
    String type;
    List<Data> dataList = new ArrayList<>();

    public Record(String type, ZonedDateTime dateTime) {
        this.type = type;
        this.dateTime = dateTime;
        ZonedDateTime dateTimeSystem = ZonedDateTime.ofInstant(dateTime.toInstant(), ZoneId.systemDefault());
        this.system = DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(dateTimeSystem);
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    public String getSystem() {
        return system;
    }

    public String getType() {
        return type;
    }

    public List<Data> getDataList() {
        return dataList;
    }

    public void setDataList(List<Data> dataList) {
        this.dataList = dataList;
    }

    public void add(Data data) {
        dataList.add(data);
    }
}
