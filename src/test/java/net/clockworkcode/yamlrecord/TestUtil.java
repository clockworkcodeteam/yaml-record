package net.clockworkcode.yamlrecord;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

/**
 * Created by david on 7/03/2016.
 */
public class TestUtil {
    public static String randomLengthAlphaNumeric(int minCharCount, int maxCharCount) {
        return RandomStringUtils.randomAlphanumeric(RandomUtils.nextInt(minCharCount, maxCharCount));
    }

}
