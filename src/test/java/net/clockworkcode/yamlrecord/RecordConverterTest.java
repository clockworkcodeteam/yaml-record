package net.clockworkcode.yamlrecord;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by david on 28/02/2016.
 */
public class RecordConverterTest {
    String type;
    String units1;
    String label1;
    BigDecimal value1;
    String units2;
    String label2;
    BigDecimal value2;
    ZonedDateTime now;
    @Before
    public void beforeEachTest() {
        type = randomAlphaNumeric();
        units1 = randomAlphaNumeric();
        label1 = randomAlphaNumeric();
        units2 = randomAlphaNumeric();
        label2 = randomAlphaNumeric();
        value1 = new BigDecimal(RandomUtils.nextDouble(100000, 900000)).setScale(6, BigDecimal.ROUND_HALF_UP);
        value2 = new BigDecimal(RandomUtils.nextDouble(1, 10)).setScale(6, BigDecimal.ROUND_HALF_UP);
        now = ZonedDateTime.now();
    }
    @Test
    public void testConversionNoRecords() throws Exception {
        Record record = new Record(type, now);
        String yaml = RecordConverter.toYaml(record);
        Record recordRead = RecordConverter.toRecord(yaml);
        assertEquals(type, recordRead.getType());
        assertTrue(now.isEqual(recordRead.getDateTime()));
        assertEquals(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(now), recordRead.getSystem());
        assertEquals(0, recordRead.getDataList().size());
    }

    @Test
    public void testConversionSingleRecord() throws Exception {
        Record record = new Record(type, now);
        record.add(new Data(label1, value1, units1));
        String yaml = RecordConverter.toYaml(record);
        Record recordRead = RecordConverter.toRecord(yaml);
        assertEquals(type, recordRead.getType());
        assertTrue(now.isEqual(recordRead.getDateTime()));
        assertEquals(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(now), recordRead.getSystem());
        assertEquals(1, recordRead.getDataList().size());
        Data data = recordRead.getDataList().get(0);
        assertEquals(label1, data.getLabel());
        checkValue(value1, data.getValue());
        assertEquals(units1, data.getUnit());
    }
    @Test
    public void testConversionMultipleRecords() throws Exception {
        Record record = new Record(type, now);
        record.add(new Data(label1, value1, units1));
        record.add(new Data(label2, value2, units2));
        String yaml = RecordConverter.toYaml(record);
        Record recordRead = RecordConverter.toRecord(yaml);
        assertEquals(type, recordRead.getType());
        assertTrue(now.isEqual(recordRead.getDateTime()));
        assertEquals(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(now), recordRead.getSystem());
        assertEquals(2, recordRead.getDataList().size());
        Data data = recordRead.getDataList().get(0);
        assertEquals(label1, data.getLabel());
        checkValue(value1, data.getValue());
        assertEquals(units1, data.getUnit());
        data = recordRead.getDataList().get(1);
        assertEquals(label2, data.getLabel());
        checkValue(value2, data.getValue());
        assertEquals(units2, data.getUnit());
    }
    @Test
    public void testBrokenRecordMissingValues() throws Exception {
        String yaml = "---\n" +
                "Type: " + type +"\n" +
                "Date: " + now +
                "\n" +
                "Data:\n" +
                "- Label: " + label1 +
                "\n" +
                "  Value: " + value1 +
                "\n" +
                "  Unit: " + units1 +
                "\n" +
                "- Label: \n";
        Record recordRead = RecordConverter.toRecord(yaml);
        assertEquals(type, recordRead.getType());
        assertTrue(now.isEqual(recordRead.getDateTime()));
        assertEquals(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(now), recordRead.getSystem());
        assertEquals(1, recordRead.getDataList().size());
        Data data = recordRead.getDataList().get(0);
        assertEquals(label1, data.getLabel());
        checkValue(value1, data.getValue());
        assertEquals(units1, data.getUnit());
    }

    private void checkValue(BigDecimal expected, BigDecimal actual) {
        assertEquals(0, expected.compareTo(actual));
    }

    private String randomAlphaNumeric() {
        return TestUtil.randomLengthAlphaNumeric(1,10);
    }
}